//
// Created by necrobuther on 15.02.2020.
//

#ifndef INC_0_BOXEXCEPTION_H
#define INC_0_BOXEXCEPTION_H

#include <bits/exception.h>
#include <string>
#include <iostream>
#include <utility>

class box_exception: public std::exception{
public:
    explicit box_exception(std::string error):message(std::move(error)){};
    [[nodiscard]] const char* what() const noexcept override { return message.c_str(); }
private:
    std::string message;
};

#endif //INC_0_BOXEXCEPTION_H
