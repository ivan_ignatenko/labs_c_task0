//
// Created by necrobuther on 17.02.2020.
//
#include "ArrayBox.h"
int sum_all_box_cost(box *array,int size){
    int sum = 0;
    if(array==nullptr){
        throw box_exception("\n""nullptr");
    }
    if(size<1){
        throw box_exception("\n"
            "unacceptable size");
    }
    for(int i = 0;i<size;i++){
            sum+=array[i].getValue();
    }
    return sum;
}

bool measure_does_not_exceed(box *array,int size,int measure){
    if(array==nullptr){
        throw box_exception("\n""nullptr");
    }
    if(size<1){
        throw box_exception("\n"
                            "unacceptable size");
    }
    for(int i=0;i<size;i++){
        if((array[i].getHeight() + array[i].getLength()+array[i].getWidth())>measure){
            return false;
        }
    }
    return true;
}

double max_weight(box *array,int size,int maxV){
    if(array==nullptr){
        throw box_exception("\n""nullptr");
    }
    if(size<1){
        throw box_exception("\n"
                            "unacceptable size");
    }
    double weight = 0;
    for(int i=0;i<size;i++){
        if(array[i].getV()<maxV && array[i].getWeight()>weight){
            weight=array[i].getWeight();
        }
    }
    return weight;
}