//
// Created by necrobuther on 13.02.2020.
//

#ifndef INC_0_BOX_H
#define INC_0_BOX_H

struct box{
public:
    box(int length, int width, int height, double weight, int value) : length(length), width(width), height(height),
                                                                       weight(weight), value(value) {};

    bool operator ==(box box1);

    bool operator !=(box box1);

    bool operator >(box box1);

    bool operator <(box box1);

    [[nodiscard]] int getValue() const;

    [[nodiscard]] unsigned getLength() const;

    [[nodiscard]] unsigned getWidth() const;

    [[nodiscard]] unsigned getHeight() const;

    [[nodiscard]] double getWeight() const;

    [[nodiscard]] unsigned getV() const;

private:
    unsigned length;
    unsigned width;
    unsigned height;
    double weight;
    int value;
};

#endif //INC_0_BOX_H
