//
// Created by necrobuther on 13.02.2020.
//
#include "Box.h"
#include "BoxException.h"
#ifndef INC_0_ARRAYBOX_H
#define INC_0_ARRAYBOX_H

int sum_all_box_cost(box *array,int size);

bool measure_does_not_exceed(box *array,int size,int measure);

double max_weight(box *array,int size,int maxV);


#endif //INC_0_ARRAYBOX_H
